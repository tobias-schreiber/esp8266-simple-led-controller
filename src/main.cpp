#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
#include <TimeLib.h>
#include <WidgetRTC.h>

char blynkPass[]                              = "";
char wlanName[]                               = "";
char wlanPass[]                               = "";
int  pinSchalter                              =  5; // Entspricht beim NodeMCU 0.9 dem Pin D1


// Das sind die globalen Variablen, welche innerhalb des Programmes benutzt werden.
volatile bool  schalterWurdeGedrueckt          = false;
bool           ledIstAn                        = false;
int            farbenId                        = 0;
bool           farbeHatSichGeandert            = false;
WidgetLED      blynkLed(V0);
WidgetRTC      blynkZeit;
int            blynkBertiebszeitStart          = -1;
int            blynkBertiebszeitStop           = -1;

// Prüfe ob die aktuelle Uhrzeit innerhalb der gesetzten Betriebszeit liegt
bool istInnerhalbDerBetriebszeit() {
  bool antwort = false;

  // Wenn die Start oder die Stop-Zeit nicht gesetzt ist, ist man immer innerhalb der Betriebszeit
  if (blynkBertiebszeitStart == -1) { antwort = true; }
  if (blynkBertiebszeitStop == -1)  { antwort = true; }

  // Wenn die aktuelle Uhrzeit (in Minuten) nach der Start aber vor der Stopzeit ist, dann auch drin
  int aktuelleZeit = (hour() * 60) + minute();
  if ((blynkBertiebszeitStart < aktuelleZeit) && (aktuelleZeit < blynkBertiebszeitStop)) { antwort = true; }

  return antwort;
}

// Funktion, die ausgeführt werden soll, wenn der (HW-)Knopf gedrückt wurde
ICACHE_RAM_ATTR void hardwareSchalterAusgeloest()     { 
    // Dokumentiere das Ereigniss und vermerke es durch setzen der Variablen
    Serial.println("Hardware Schalter wurde gedrueckt.");
    schalterWurdeGedrueckt = true;
}

// BLYNK Button 
//  - Pin: V1 
//  - Mode: Switch
BLYNK_WRITE(V1) {
  int blynkSchalterIstAn = param.asInt();
  if (blynkSchalterIstAn != ledIstAn) {
    // Dokumentiere das Ereigniss und vermke es durch Setzen der Variable (schalterWurdeGedrueckt)
    Serial.println("Schalter in BLYNK wurde gedrueckt.");
    schalterWurdeGedrueckt = true;
  }
}

// BLYNK Numeric Input
//  - Pin: V2 
//  - Values: -20 / +80
BLYNK_WRITE(V2) {
  int blynkTemperatur = param.asInt();

  // Ermittele die zum Temperaturbereich gehörende FarbenId
  int farbenIdTemp = 0;
  if        (blynkTemperatur <= 0) {
    farbenIdTemp = 1;
  } else if (blynkTemperatur < 10) {
    farbenIdTemp = 2;
  } else if (blynkTemperatur < 20) {
    farbenIdTemp = 3;
  } else if (blynkTemperatur < 30) {
    farbenIdTemp = 4;
  } else if (blynkTemperatur > 30) {
    farbenIdTemp = 5;
  }  
  // Wenn sich die FarbenId geändert hat dann vermerke dies und setze die neue Farbe
  if (farbenIdTemp != farbenId) {
    farbenId = farbenIdTemp;
    farbeHatSichGeandert = true;
  }
}

// BLYNK Time Input
// - Pin: V3
// - Allow Start/Stop Input: Yes
BLYNK_WRITE(V3) {
  TimeInputParam blynkBetriebszeit(param);
  char ausgabe[80];

  // Lese den aktuellen Wert für die Startzeit aus und dokumentiere ihn (in Minuten)
  if (blynkBetriebszeit.hasStartTime()) {
    blynkBertiebszeitStart = (blynkBetriebszeit.getStartHour() * 60) + blynkBetriebszeit.getStartMinute();
    sprintf(ausgabe, "%2d:%02d", blynkBetriebszeit.getStartHour(), blynkBetriebszeit.getStartMinute());
  } else {
    blynkBertiebszeitStart = -1;
    sprintf(ausgabe, "nicht gesetzt");
  }
  Serial.print("Neuer Start der Betriebszeit:   ");
  Serial.println(ausgabe);

  // Lese den aktuellen Wert für den Endzeitpunkt aus und dokumentiere ihn (in Minuten)
  if (blynkBetriebszeit.hasStopTime()) {
    blynkBertiebszeitStop = (blynkBetriebszeit.getStopHour() * 60) + blynkBetriebszeit.getStopMinute();
    sprintf(ausgabe, "%2d:%02d", blynkBetriebszeit.getStopHour(), blynkBetriebszeit.getStopMinute());
  } else {
    blynkBertiebszeitStop = -1;
    sprintf(ausgabe, "nicht gesetzt");
  }
  Serial.print("Neues Ende der Betriebszeit:    ");
  Serial.println(ausgabe);
}

// Definiere was der BLYNK-Agent synchronisieren soll
BLYNK_CONNECTED() {
  Blynk.syncVirtual(V1);
  Blynk.syncVirtual(V2);
  blynkZeit.begin();
  Blynk.syncVirtual(V3);  
}

// Gebe die aktuelle Uhrzeit auf der Konsole aus
void schreibeUhrzeit() {
  char ausgabe[80];
  sprintf(ausgabe, "[ %2d:%02d ]> ", hour(), minute());
  Serial.print(ausgabe);  
}

void schalteLicht(bool status) {
  // Setze den Status in der LED 
  ledIstAn = status;

  // Setze den BLYNK Kippschalter auf den aktuellen Status
  Blynk.virtualWrite(V1, status);

  // Setze die LED in BLYNK
  if (status) { blynkLed.on();  }
  else        { blynkLed.off(); }

  // Dokumentiere den Status in der Ausgabe
  schreibeUhrzeit();
  if (status) { Serial.println("Die LED wurde eingeschaltet.");  }
  else        { Serial.println("Die LED ist nun AUS."); }
}

//
// Hauptprogramm
//
void setup() {
  // Beginne die serielle Ausgabe am Bildschirm
  Serial.begin(9600);
  Serial.println();
  Serial.println("System wird gestartet ...");

  // Verknüpfe Funktionen mit Zustandswechseln von PINs
  Serial.println(" - Verbinde PINs mit Funktionen");
  attachInterrupt(digitalPinToInterrupt(pinSchalter), hardwareSchalterAusgeloest, RISING);

  // Starte die Verbindung mit BLYNK
  Serial.println(" - Starte BLYNK");
  Blynk.begin(blynkPass, wlanName, wlanPass);
  Blynk.virtualWrite(V1, 0);
  blynkLed.off();

  // Setze Interval zur Zeitsynchronisation
  Serial.println(" - Setze Interval Zeit Synchronisierung (15m)");
  setSyncInterval(15 * 60); 

  // Beende den Startvorgang durch Textausgabe
  Serial.println("Start des Systems abgeschlossen.");
  Serial.println();
  schalterWurdeGedrueckt = false;
}

void loop() {
  // Führe den BYLNK Agenten aus
  Blynk.run();

  // Falls wir ausserhalb der Betriebszeit sind müssen ein paar Dinge geprüft werden. 
  if (istInnerhalbDerBetriebszeit() == false) {
    // Per Definiton wird jeder Schalterdruck ausserhalb der Betriebszeit (ohne Aktion) zurückgesetzt. 
    // Dabei muss vorsorglich auch der virtuelle "Kippschalter" in BLYNK auf ausgeschaltet werden.
    if (schalterWurdeGedrueckt) {
      schalterWurdeGedrueckt = false;
      Blynk.virtualWrite(V1, false);

      schreibeUhrzeit();
      Serial.println("Ignoriere Schalter (Ausserhalb der Betriebszeit)");
    }

    // Falls das Licht ausserhalb der Betriebszeit an ist, so wird es ausgeschaltet
    if (ledIstAn) {
      schalteLicht(false);
    }
  }

// Prüfe ob ein Schalter gedrückt wurde, führe dann die notwendigen Aktionen aus
  if (schalterWurdeGedrueckt) {
    // Setze das Ereigniss wieder zurück
    schalterWurdeGedrueckt = false;

    // Setze den Status der LED auf das Gegenteil von dem was es aktuell ist
    // not AN == AUS // not AUS == AN
    schalteLicht(not ledIstAn);
  }

  // Prüfe ob sich die FarbenId geändert hat und führe dann die notwendingen Aktionen aus
  if (farbeHatSichGeandert) {
    // Setze das Ereigniss wieder zurück
    farbeHatSichGeandert = false;

    // Setze die Farbe in der BLYNK LED und gebe ihren Namen auf der Console aus
    schreibeUhrzeit();   
    if        (farbenId == 1) {
      Serial.println("Die Farbe ist nun WEISS (bzw. SCHWARZ)");
      blynkLed.setColor("#000000");
    } else if (farbenId == 2) {
      Serial.println("Die Farbe ist nun BLAU");
      blynkLed.setColor("#04C0F8");
    } else if (farbenId == 3) {
      Serial.println("Die Farbe ist nun GRUEN");
      blynkLed.setColor("#23C48E");
    } else if (farbenId == 4) {
      Serial.println("Die Farbe ist nun GELB");
      blynkLed.setColor("#ED9D00");
    } else if (farbenId == 5) {
      Serial.println("Die Farbe ist nun ROT");
      blynkLed.setColor("#D3435C");
    }
  }

}
